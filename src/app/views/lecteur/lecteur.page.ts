import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AudioService } from 'src/app/providers/audio.service';

@Component({
  selector: 'app-lecteur',
  templateUrl: './lecteur.page.html',
  styleUrls: ['./lecteur.page.scss'],
})
export class LecteurPage implements OnInit {

  Music : any;
  Album : any;
  List = [];
  pause: boolean;
  vol: any;
  time: any;

  constructor(public activate: ActivatedRoute, private router:Router, private audioService: AudioService) { 
    this.vol = 0.5;
    this.pause = false;
  }

  ionViewWillEnter(){
    this.loadMusic(this.Music);
    //this.duree(this.Album);
  }

  // getAudio(){
  //   this.audioService.getSons().subscribe((reponse: any[])=>{
  //     this.List = reponse.filter(A => {
  //       return A.album.indexOf(this.Album.id) > -1
  //     }) ;
  //   });
  // }


  async PlayMusic(aud){
 
    this.audioService.getPlay(aud.id);

  }

  StopMusic(aud){

    this.audioService.getStop(aud.id);

  }

  VolumeAdd(aud){

    if(this.vol<0.9){
        this.vol = this.vol + 0.1;
        this.audioService.getVolume(aud.id, this.vol);

    }

  }

  VolumeSub(aud){

    if(0.1<this.vol){
        this.vol = this.vol - 0.1;
        this.audioService.getVolume(aud.id, this.vol);
      }

  }

  ResumeMusic(aud){
    this.audioService.getResume(aud.id);
    this.pause = false;
  }

  PauseMusic(aud){
    this.audioService.getPause(aud.id);
    this.pause = true;
    
  }

   async duree(aud){
      await this.audioService.DureeMusic(aud.id).then((response:any)=>{
       this.time = response.duration;
       console.log(this.time);
      },err => {
       console.log(err.message);
      });
    }

  ngOnInit() {
    
    let music = localStorage.getItem("music");
    let album = localStorage.getItem("alb");
    this.Music = JSON.parse(music);
    this.Album = JSON.parse(album);
    this.loadMusic(this.Music);
    
    // this.duree(this.Music.id);
  }

  async loadMusic(audio){
    await this.audioService.getAudio(audio.id, audio.path);
    this.duree(audio);
  }

}
