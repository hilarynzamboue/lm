import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlbumService } from 'src/app/providers/album.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {

  Album = [];

  constructor(private router:Router, private album: AlbumService) { }

  onClick(a){
    localStorage.setItem("album", JSON.stringify(a));
    this.router.navigate(['/music']);
  }

  getListOfData(){
    this.album.getAlbum().subscribe((reponse: any)=>{
      this.Album = reponse;
    });
  }
 

  ngOnInit() {
    this.getListOfData();
  }

}
