import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AudioService } from 'src/app/providers/audio.service';

@Component({
  selector: 'app-music',
  templateUrl: './music.page.html',
  styleUrls: ['./music.page.scss'],
})
export class MusicPage implements OnInit {

  constructor(private audioService: AudioService, public activate: ActivatedRoute, private router:Router) { }

  List = [];
  Album : any;

  onClick(m, a){
    localStorage.setItem("music", JSON.stringify(m));
    localStorage.setItem("alb", JSON.stringify(a));
    this.router.navigate(['/lecteur']);
  }

  getAudio(){
    this.audioService.getSons().subscribe((reponse: any[])=>{
      this.List = reponse.filter(A => {
        return A.album.indexOf(this.Album.id) > -1
      }) ;
    });
  }

  ngOnInit() {
    this.getAudio();
    let album = localStorage.getItem("album");
    this.Album = JSON.parse(album);
  }

}
