import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  constructor(private http: HttpClient) { }

  chemin = "assets/album.json";

  getAlbum(){
    return this.http.get(this.chemin);
  }

}
