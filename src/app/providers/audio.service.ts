import { Injectable } from '@angular/core';
import { NativeAudio } from '@capacitor-community/native-audio';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  constructor(private http: HttpClient) { }

  chemin = "assets/sons.json";

  getSons(){
    return this.http.get(this.chemin);
  }

  getAudio(audioId, audioPath){
    NativeAudio.preload({
      assetId: audioId,
      assetPath: audioPath,
      audioChannelNum: 1,
      isUrl: false
    });
  }

  getPlay(audioId){
    NativeAudio.play({
      assetId: audioId,
      time: 0
    });
  }

  getStop(audioId){
    NativeAudio.stop({
      assetId: audioId
    });
  }

  getLoop(audioId){
    NativeAudio.loop({
      assetId: audioId
    });
  }

  getVolume(audioId, volume){
    NativeAudio.setVolume({
      assetId: audioId,
      volume: volume
      });
  }

  getResume(audioId){
    NativeAudio.resume({
      assetId: audioId,
      });
  }

  getPause(audioId){
    NativeAudio.pause({
      assetId: audioId,
      });
  }

  DureeMusic(audioId){
      return NativeAudio.getDuration({
        assetId: audioId
      });
  }

}
